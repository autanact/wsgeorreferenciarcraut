
/**
 * ServiciosSIGCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package sig;

    /**
     *  ServiciosSIGCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServiciosSIGCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServiciosSIGCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServiciosSIGCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for wsGetInfoServiciosPublicos method
            * override this method for handling normal response from wsGetInfoServiciosPublicos operation
            */
           public void receiveResultwsGetInfoServiciosPublicos(
                    sig.ServiciosSIGStub.WsGetInfoServiciosPublicosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfoServiciosPublicos operation
           */
            public void receiveErrorwsGetInfoServiciosPublicos(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeoreferenciarPR method
            * override this method for handling normal response from wsGeoreferenciarPR operation
            */
           public void receiveResultwsGeoreferenciarPR(
                    sig.ServiciosSIGStub.WsGeoreferenciarPRResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeoreferenciarPR operation
           */
            public void receiveErrorwsGeoreferenciarPR(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsRetornarRespuestaGeorreferenciadorTecnocom method
            * override this method for handling normal response from wsRetornarRespuestaGeorreferenciadorTecnocom operation
            */
           public void receiveResultwsRetornarRespuestaGeorreferenciadorTecnocom(
                    sig.ServiciosSIGStub.WsRetornarRespuestaGeorreferenciadorTecnocomResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsRetornarRespuestaGeorreferenciadorTecnocom operation
           */
            public void receiveErrorwsRetornarRespuestaGeorreferenciadorTecnocom(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetSitiosInteresCompleto method
            * override this method for handling normal response from wsGetSitiosInteresCompleto operation
            */
           public void receiveResultwsGetSitiosInteresCompleto(
                    sig.ServiciosSIGStub.WsGetSitiosInteresCompletoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetSitiosInteresCompleto operation
           */
            public void receiveErrorwsGetSitiosInteresCompleto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetMunicipios method
            * override this method for handling normal response from wsGetMunicipios operation
            */
           public void receiveResultwsGetMunicipios(
                    sig.ServiciosSIGStub.WsGetMunicipiosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetMunicipios operation
           */
            public void receiveErrorwsGetMunicipios(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetTraceRed method
            * override this method for handling normal response from wsGetTraceRed operation
            */
           public void receiveResultwsGetTraceRed(
                    sig.ServiciosSIGStub.WsGetTraceRedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetTraceRed operation
           */
            public void receiveErrorwsGetTraceRed(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeorreferenciarCRMO method
            * override this method for handling normal response from wsGeorreferenciarCRMO operation
            */
           public void receiveResultwsGeorreferenciarCRMO(
                    sig.ServiciosSIGStub.WsGeorreferenciarCRMOResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeorreferenciarCRMO operation
           */
            public void receiveErrorwsGeorreferenciarCRMO(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfluenciaWimax method
            * override this method for handling normal response from wsGetInfluenciaWimax operation
            */
           public void receiveResultwsGetInfluenciaWimax(
                    sig.ServiciosSIGStub.WsGetInfluenciaWimaxResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfluenciaWimax operation
           */
            public void receiveErrorwsGetInfluenciaWimax(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCadenaGeoreferenciacion method
            * override this method for handling normal response from getCadenaGeoreferenciacion operation
            */
           public void receiveResultgetCadenaGeoreferenciacion(
                    sig.ServiciosSIGStub.GetCadenaGeoreferenciacionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCadenaGeoreferenciacion operation
           */
            public void receiveErrorgetCadenaGeoreferenciacion(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeoreferenciarSTRING method
            * override this method for handling normal response from wsGeoreferenciarSTRING operation
            */
           public void receiveResultwsGeoreferenciarSTRING(
                    sig.ServiciosSIGStub.WsGeoreferenciarSTRINGResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeoreferenciarSTRING operation
           */
            public void receiveErrorwsGeoreferenciarSTRING(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeorreferenciarCR method
            * override this method for handling normal response from wsGeorreferenciarCR operation
            */
           public void receiveResultwsGeorreferenciarCR(
                    sig.ServiciosSIGStub.WsGeorreferenciarCRResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeorreferenciarCR operation
           */
            public void receiveErrorwsGeorreferenciarCR(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetSitiosInteres method
            * override this method for handling normal response from wsGetSitiosInteres operation
            */
           public void receiveResultwsGetSitiosInteres(
                    sig.ServiciosSIGStub.WsGetSitiosInteresResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetSitiosInteres operation
           */
            public void receiveErrorwsGetSitiosInteres(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeorreferenciarElite method
            * override this method for handling normal response from wsGeorreferenciarElite operation
            */
           public void receiveResultwsGeorreferenciarElite(
                    sig.ServiciosSIGStub.WsGeorreferenciarEliteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeorreferenciarElite operation
           */
            public void receiveErrorwsGeorreferenciarElite(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsObtenerCodificacionNLectura method
            * override this method for handling normal response from wsObtenerCodificacionNLectura operation
            */
           public void receiveResultwsObtenerCodificacionNLectura(
                    sig.ServiciosSIGStub.WsObtenerCodificacionNLecturaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsObtenerCodificacionNLectura operation
           */
            public void receiveErrorwsObtenerCodificacionNLectura(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsRetornarRespuestasGeorreferenciadorTecnocom method
            * override this method for handling normal response from wsRetornarRespuestasGeorreferenciadorTecnocom operation
            */
           public void receiveResultwsRetornarRespuestasGeorreferenciadorTecnocom(
                    sig.ServiciosSIGStub.WsRetornarRespuestasGeorreferenciadorTecnocomResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsRetornarRespuestasGeorreferenciadorTecnocom operation
           */
            public void receiveErrorwsRetornarRespuestasGeorreferenciadorTecnocom(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeorreferenciarCRSTRING method
            * override this method for handling normal response from wsGeorreferenciarCRSTRING operation
            */
           public void receiveResultwsGeorreferenciarCRSTRING(
                    sig.ServiciosSIGStub.WsGeorreferenciarCRSTRINGResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeorreferenciarCRSTRING operation
           */
            public void receiveErrorwsGeorreferenciarCRSTRING(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfluenciaLTECoord method
            * override this method for handling normal response from wsGetInfluenciaLTECoord operation
            */
           public void receiveResultwsGetInfluenciaLTECoord(
                    sig.ServiciosSIGStub.WsGetInfluenciaLTECoordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfluenciaLTECoord operation
           */
            public void receiveErrorwsGetInfluenciaLTECoord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarFactibilidadCobre method
            * override this method for handling normal response from wsConsultarFactibilidadCobre operation
            */
           public void receiveResultwsConsultarFactibilidadCobre(
                    sig.ServiciosSIGStub.WsConsultarFactibilidadCobreResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarFactibilidadCobre operation
           */
            public void receiveErrorwsConsultarFactibilidadCobre(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfoMovimeintoMasa method
            * override this method for handling normal response from wsGetInfoMovimeintoMasa operation
            */
           public void receiveResultwsGetInfoMovimeintoMasa(
                    sig.ServiciosSIGStub.WsGetInfoMovimeintoMasaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfoMovimeintoMasa operation
           */
            public void receiveErrorwsGetInfoMovimeintoMasa(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsInfoCartografia method
            * override this method for handling normal response from wsInfoCartografia operation
            */
           public void receiveResultwsInfoCartografia(
                    sig.ServiciosSIGStub.WsInfoCartografiaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsInfoCartografia operation
           */
            public void receiveErrorwsInfoCartografia(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsObtenerDireccionesExcepcionadas method
            * override this method for handling normal response from wsObtenerDireccionesExcepcionadas operation
            */
           public void receiveResultwsObtenerDireccionesExcepcionadas(
                    sig.ServiciosSIGStub.WsObtenerDireccionesExcepcionadasResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsObtenerDireccionesExcepcionadas operation
           */
            public void receiveErrorwsObtenerDireccionesExcepcionadas(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfluenciaGpon method
            * override this method for handling normal response from wsGetInfluenciaGpon operation
            */
           public void receiveResultwsGetInfluenciaGpon(
                    sig.ServiciosSIGStub.WsGetInfluenciaGponResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfluenciaGpon operation
           */
            public void receiveErrorwsGetInfluenciaGpon(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarEjemplo method
            * override this method for handling normal response from wsConsultarEjemplo operation
            */
           public void receiveResultwsConsultarEjemplo(
                    sig.ServiciosSIGStub.WsConsultarEjemploResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarEjemplo operation
           */
            public void receiveErrorwsConsultarEjemplo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarDirExcepRuralesRes method
            * override this method for handling normal response from wsConsultarDirExcepRuralesRes operation
            */
           public void receiveResultwsConsultarDirExcepRuralesRes(
                    sig.ServiciosSIGStub.WsConsultarDirExcepRuralesResResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarDirExcepRuralesRes operation
           */
            public void receiveErrorwsConsultarDirExcepRuralesRes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for HelloWorld method
            * override this method for handling normal response from HelloWorld operation
            */
           public void receiveResultHelloWorld(
                    sig.ServiciosSIGStub.HelloWorldResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from HelloWorld operation
           */
            public void receiveErrorHelloWorld(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarDireccionCriterio method
            * override this method for handling normal response from wsConsultarDireccionCriterio operation
            */
           public void receiveResultwsConsultarDireccionCriterio(
                    sig.ServiciosSIGStub.WsConsultarDireccionCriterioResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarDireccionCriterio operation
           */
            public void receiveErrorwsConsultarDireccionCriterio(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetOpcionesUsuarioSIG method
            * override this method for handling normal response from wsGetOpcionesUsuarioSIG operation
            */
           public void receiveResultwsGetOpcionesUsuarioSIG(
                    sig.ServiciosSIGStub.WsGetOpcionesUsuarioSIGResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetOpcionesUsuarioSIG operation
           */
            public void receiveErrorwsGetOpcionesUsuarioSIG(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarEstadisticasDirExcep method
            * override this method for handling normal response from wsConsultarEstadisticasDirExcep operation
            */
           public void receiveResultwsConsultarEstadisticasDirExcep(
                    sig.ServiciosSIGStub.WsConsultarEstadisticasDirExcepResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarEstadisticasDirExcep operation
           */
            public void receiveErrorwsConsultarEstadisticasDirExcep(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarUsuarioSIGWEB method
            * override this method for handling normal response from wsConsultarUsuarioSIGWEB operation
            */
           public void receiveResultwsConsultarUsuarioSIGWEB(
                    sig.ServiciosSIGStub.WsConsultarUsuarioSIGWEBResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarUsuarioSIGWEB operation
           */
            public void receiveErrorwsConsultarUsuarioSIGWEB(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeoreferenciar method
            * override this method for handling normal response from wsGeoreferenciar operation
            */
           public void receiveResultwsGeoreferenciar(
                    sig.ServiciosSIGStub.WsGeoreferenciarResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeoreferenciar operation
           */
            public void receiveErrorwsGeoreferenciar(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for WsEstadoProyecto method
            * override this method for handling normal response from WsEstadoProyecto operation
            */
           public void receiveResultWsEstadoProyecto(
                    sig.ServiciosSIGStub.WsEstadoProyectoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from WsEstadoProyecto operation
           */
            public void receiveErrorWsEstadoProyecto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsCancelarDireccionExc method
            * override this method for handling normal response from wsCancelarDireccionExc operation
            */
           public void receiveResultwsCancelarDireccionExc(
                    sig.ServiciosSIGStub.WsCancelarDireccionExcResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsCancelarDireccionExc operation
           */
            public void receiveErrorwsCancelarDireccionExc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarFactibilidad4G method
            * override this method for handling normal response from wsConsultarFactibilidad4G operation
            */
           public void receiveResultwsConsultarFactibilidad4G(
                    sig.ServiciosSIGStub.WsConsultarFactibilidad4GResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarFactibilidad4G operation
           */
            public void receiveErrorwsConsultarFactibilidad4G(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for WsCambioProyecto method
            * override this method for handling normal response from WsCambioProyecto operation
            */
           public void receiveResultWsCambioProyecto(
                    sig.ServiciosSIGStub.WsCambioProyectoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from WsCambioProyecto operation
           */
            public void receiveErrorWsCambioProyecto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfoCondicionesRiesgo method
            * override this method for handling normal response from wsGetInfoCondicionesRiesgo operation
            */
           public void receiveResultwsGetInfoCondicionesRiesgo(
                    sig.ServiciosSIGStub.WsGetInfoCondicionesRiesgoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfoCondicionesRiesgo operation
           */
            public void receiveErrorwsGetInfoCondicionesRiesgo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsActualizarDireccionExcepcionada method
            * override this method for handling normal response from wsActualizarDireccionExcepcionada operation
            */
           public void receiveResultwsActualizarDireccionExcepcionada(
                    sig.ServiciosSIGStub.WsActualizarDireccionExcepcionadaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsActualizarDireccionExcepcionada operation
           */
            public void receiveErrorwsActualizarDireccionExcepcionada(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGeorreferenciarCRPrueba method
            * override this method for handling normal response from wsGeorreferenciarCRPrueba operation
            */
           public void receiveResultwsGeorreferenciarCRPrueba(
                    sig.ServiciosSIGStub.WsGeorreferenciarCRPruebaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGeorreferenciarCRPrueba operation
           */
            public void receiveErrorwsGeorreferenciarCRPrueba(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetFactibilidadServiciosSTRING method
            * override this method for handling normal response from wsGetFactibilidadServiciosSTRING operation
            */
           public void receiveResultwsGetFactibilidadServiciosSTRING(
                    sig.ServiciosSIGStub.WsGetFactibilidadServiciosSTRINGResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetFactibilidadServiciosSTRING operation
           */
            public void receiveErrorwsGetFactibilidadServiciosSTRING(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarUsuarioDirActivo method
            * override this method for handling normal response from wsConsultarUsuarioDirActivo operation
            */
           public void receiveResultwsConsultarUsuarioDirActivo(
                    sig.ServiciosSIGStub.WsConsultarUsuarioDirActivoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarUsuarioDirActivo operation
           */
            public void receiveErrorwsConsultarUsuarioDirActivo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfluenciaLTE method
            * override this method for handling normal response from wsGetInfluenciaLTE operation
            */
           public void receiveResultwsGetInfluenciaLTE(
                    sig.ServiciosSIGStub.WsGetInfluenciaLTEResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfluenciaLTE operation
           */
            public void receiveErrorwsGetInfluenciaLTE(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarGIVentas method
            * override this method for handling normal response from wsConsultarGIVentas operation
            */
           public void receiveResultwsConsultarGIVentas(
                    sig.ServiciosSIGStub.WsConsultarGIVentasResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarGIVentas operation
           */
            public void receiveErrorwsConsultarGIVentas(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarDireccionExc method
            * override this method for handling normal response from wsConsultarDireccionExc operation
            */
           public void receiveResultwsConsultarDireccionExc(
                    sig.ServiciosSIGStub.WsConsultarDireccionExcResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarDireccionExc operation
           */
            public void receiveErrorwsConsultarDireccionExc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetFactibilidadServicios method
            * override this method for handling normal response from wsGetFactibilidadServicios operation
            */
           public void receiveResultwsGetFactibilidadServicios(
                    sig.ServiciosSIGStub.WsGetFactibilidadServiciosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetFactibilidadServicios operation
           */
            public void receiveErrorwsGetFactibilidadServicios(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsConsultarDireccionExcSolucionada method
            * override this method for handling normal response from wsConsultarDireccionExcSolucionada operation
            */
           public void receiveResultwsConsultarDireccionExcSolucionada(
                    sig.ServiciosSIGStub.WsConsultarDireccionExcSolucionadaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsConsultarDireccionExcSolucionada operation
           */
            public void receiveErrorwsConsultarDireccionExcSolucionada(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsObtenerDireccionesExcepcionadasFecha method
            * override this method for handling normal response from wsObtenerDireccionesExcepcionadasFecha operation
            */
           public void receiveResultwsObtenerDireccionesExcepcionadasFecha(
                    sig.ServiciosSIGStub.WsObtenerDireccionesExcepcionadasFechaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsObtenerDireccionesExcepcionadasFecha operation
           */
            public void receiveErrorwsObtenerDireccionesExcepcionadasFecha(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsReportarDireccionExc method
            * override this method for handling normal response from wsReportarDireccionExc operation
            */
           public void receiveResultwsReportarDireccionExc(
                    sig.ServiciosSIGStub.WsReportarDireccionExcResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsReportarDireccionExc operation
           */
            public void receiveErrorwsReportarDireccionExc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCadenaGeoreferenciacionSistemaCoord method
            * override this method for handling normal response from getCadenaGeoreferenciacionSistemaCoord operation
            */
           public void receiveResultgetCadenaGeoreferenciacionSistemaCoord(
                    sig.ServiciosSIGStub.GetCadenaGeoreferenciacionSistemaCoordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCadenaGeoreferenciacionSistemaCoord operation
           */
            public void receiveErrorgetCadenaGeoreferenciacionSistemaCoord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsFinalizarDireccionExc method
            * override this method for handling normal response from wsFinalizarDireccionExc operation
            */
           public void receiveResultwsFinalizarDireccionExc(
                    sig.ServiciosSIGStub.WsFinalizarDireccionExcResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsFinalizarDireccionExc operation
           */
            public void receiveErrorwsFinalizarDireccionExc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetUsuarioSIG method
            * override this method for handling normal response from wsGetUsuarioSIG operation
            */
           public void receiveResultwsGetUsuarioSIG(
                    sig.ServiciosSIGStub.WsGetUsuarioSIGResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetUsuarioSIG operation
           */
            public void receiveErrorwsGetUsuarioSIG(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsGetInfluencia method
            * override this method for handling normal response from wsGetInfluencia operation
            */
           public void receiveResultwsGetInfluencia(
                    sig.ServiciosSIGStub.WsGetInfluenciaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsGetInfluencia operation
           */
            public void receiveErrorwsGetInfluencia(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for WsRespuestaPC method
            * override this method for handling normal response from WsRespuestaPC operation
            */
           public void receiveResultWsRespuestaPC(
                    sig.ServiciosSIGStub.WsRespuestaPCResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from WsRespuestaPC operation
           */
            public void receiveErrorWsRespuestaPC(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsObtenerEncapsulamientoDireccion method
            * override this method for handling normal response from wsObtenerEncapsulamientoDireccion operation
            */
           public void receiveResultwsObtenerEncapsulamientoDireccion(
                    sig.ServiciosSIGStub.WsObtenerEncapsulamientoDireccionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsObtenerEncapsulamientoDireccion operation
           */
            public void receiveErrorwsObtenerEncapsulamientoDireccion(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wsResponderDireccionExcepcionada method
            * override this method for handling normal response from wsResponderDireccionExcepcionada operation
            */
           public void receiveResultwsResponderDireccionExcepcionada(
                    sig.ServiciosSIGStub.WsResponderDireccionExcepcionadaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wsResponderDireccionExcepcionada operation
           */
            public void receiveErrorwsResponderDireccionExcepcionada(java.lang.Exception e) {
            }
                


    }
    