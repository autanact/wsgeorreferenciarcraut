
/**
 * WSGeorreferenciarCRServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSGeorreferenciarCRServiceSkeleton java skeleton for the axisService
     */
    public class WSGeorreferenciarCRServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSGeorreferenciarCRServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoPais
                                     * @param codigoDepartamento
                                     * @param codigoMunicipio
                                     * @param direccionNatural
                                     * @param latitud
                                     * @param longitud
                                     * @param uNE_Cobertura_Especial
         */
        

                 public co.net.une.www.gis.WSGeorreferenciarCRRSType georeferenciarCR
                  (
                  java.lang.String codigoPais,java.lang.String codigoDepartamento,java.lang.String codigoMunicipio,java.lang.String direccionNatural,java.lang.String latitud,java.lang.String longitud,int uNE_Cobertura_Especial
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoPais",codigoPais);params.put("codigoDepartamento",codigoDepartamento);params.put("codigoMunicipio",codigoMunicipio);params.put("direccionNatural",direccionNatural);params.put("latitud",latitud);params.put("longitud",longitud);params.put("uNE_Cobertura_Especial",uNE_Cobertura_Especial);
		try{
		
			return (co.net.une.www.gis.WSGeorreferenciarCRRSType)
			this.makeStructuredRequest(serviceName, "georeferenciarCR", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    